export const publicRoutes = FlowRouter.group({
	name:'publicRoutes'
});

export const privateRoutes = FlowRouter.group({
	name:'privateRoutes',
	triggersEnter:[
		function(obj,redirect){
			if (!Meteor.userId()){
				Bert.alert('Você precisa fazer o login para ter permissão de acesso.','danger','fixed-bottom');
				redirect('loginRoute');
			}
		}
	]
});
