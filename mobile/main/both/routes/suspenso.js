import { privateRoutes,publicRoutes } from './groups.js';

privateRoutes.route('/suspenso',{
	name: 'suspensoRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'suspensoView',
			technotronics:'technotronicsMenu'
		});
	}

});
