import { privateRoutes,publicRoutes } from './groups.js';

privateRoutes.route('/galerias',{
	name: 'galeriasRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'galeriasView',
			technotronics:'technotronicsMenu'
		});
	}

});

privateRoutes.route('/galerias/:id',{
	name: 'fotosRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'fotosView',
			technotronics:'technotronicsMenu'
		});
	}

});
