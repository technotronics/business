import { privateRoutes,publicRoutes } from './groups.js';

privateRoutes.route('/noticias',{
	name: 'noticiasRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'noticiasView',
			technotronics:'technotronicsMenu'
		});
	}
});

privateRoutes.route('/noticias/:id',{
	name: 'noticiasDetalheRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'noticiasDetalheView',
			technotronics:'technotronicsMenu'
		});
	}
});
