import { privateRoutes,publicRoutes } from './groups.js';

FlowRouter.notFound = {
  action: function() {
    BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'notFoundView',
			technotronics:'technotronicsMenu'
		});
  }
};
publicRoutes.route('/',{
	name: 'homeRoute',
	triggersEnter:[
		function(route,redirect){
			if(!Meteor.isCordova){
				if(Meteor.Device.isDesktop()){
					if (window.parent == window){
						redirect('/desktop');
					}
				}
			}
		}
	],
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'homeView',
			technotronics:'technotronicsMenu'
		});
	}
});
publicRoutes.route('/desktop',{
	name: 'desktopRoute',
	triggersEnter:[
		function(route,redirect){
			$('body').css('background-color','#989898');
		}
	],
	action: function() {
		BlazeLayout.render('mobileEmulator');
	}
});
