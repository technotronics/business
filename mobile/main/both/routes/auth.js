import { privateRoutes,publicRoutes } from './groups.js';

publicRoutes.route('/login',{
	name: 'loginRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'loginView',
			technotronics:'technotronicsMenu'
		});
	}
});

publicRoutes.route('/login-pwd',{
	name: 'loginPwdRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'loginPwdView',
			technotronics:'technotronicsMenu'
		});
	}
});

publicRoutes.route('/registro',{
	name: 'registerRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'registerView',
			technotronics:'technotronicsMenu'
		});
	}

});

publicRoutes.route('/logout',{
	name: 'logoutRoute',
	triggersEnter:[
		function(){
			Meteor.logout();
		}
	]
});
