import { privateRoutes,publicRoutes } from './groups.js';

privateRoutes.route('/aniversarios',{
	name: 'aniversariosRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'aniversariosView',
			technotronics:'technotronicsMenu'
		});
	}

});
