import { privateRoutes,publicRoutes } from './groups.js';

privateRoutes.route('/conversas',{
	name: 'conversasRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'conversasView',
			technotronics:'technotronicsMenu'
		});
	}
});

privateRoutes.route('/conversas/:id',{
	name: 'mensagensRoute',
	action: function() {
		BlazeLayout.render('adminLayout',{
			menu:'topMenu',
			main:'mensagensView',
			technotronics:'technotronicsMenu'
		});
	}
});
