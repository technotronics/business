Meteor.users.before.insert(function (userId, doc) {
	if (doc.services.google) {
		doc.emails = [
			{
				address:doc.services.google.email,
				verified:true
			}
		];
	}
	return doc;
});
Meteor.users.after.insert(function (userId, doc) {
	Roles.addUsersToRoles(doc._id, ['Associado'],'user-type');
	Roles.addUsersToRoles(doc._id, ['Novo'],'user-status');
});
