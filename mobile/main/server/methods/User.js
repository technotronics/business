Meteor.methods({
	registerUser:function(fields,fieldsTwo){
		var userObject = {
			email: fields.email,
			password: fields.password1,
			profile:{
				name:fieldsTwo.name,
				birthday:moment(fieldsTwo.birthday,'DD/MM/YYYY').toDate(),
				birthday_month:moment(fieldsTwo.birthday,'DD/MM/YYYY').format('MM')
			}
		};
		var id = Accounts.createUser(userObject);
		Roles.addUsersToRoles(id, ['Associado'],'user-type');
		Roles.addUsersToRoles(id, ['Novo'],'user-state');
		return id;
	}
});
