Meteor.publish('oneNoticia',function(id){
  var noticia = Noticia.find({_id:id,entidade_id:Meteor.settings.public.entidade_id});
	return noticia;
});
Meteor.publish('pubNoticias',function(){
  var noticias = Noticia.find({publicada:'1',entidade_id:Meteor.settings.public.entidade_id},{sort:{date:-1}});
	return noticias;
});
