Meteor.publishComposite('allGalerias', function(id){
	return {
		find:function(){
			return Galeria.find(
				{
					entidade_id:Meteor.settings.public.entidade_id,
					publicada:'1',
				},
				{sort:{date:-1}}
			);
		},
		children:[
			{
				find:function(galeria){
					return Foto.find(
						{
							entidade_id:Meteor.settings.public.entidade_id,
							galeria_id:galeria._id
						}
					);
				}
			}
		]
	}
});
Meteor.publish('galeriaFotos',function(galeria_id){
  var fotos = Foto.find(
		{
			entidade_id:Meteor.settings.public.entidade_id,
			galeria_id:galeria_id
		}
	);
	return fotos;
});
