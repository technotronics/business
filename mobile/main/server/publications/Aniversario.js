Meteor.publishComposite('allAniversariantes', function(){
	return {
		find:function(){
			var month = moment().format('MM');
			var search = {
				'profile.entidade_id':Meteor.settings.public.entidade_id,
				'profile.birthday_month':month
			};
			var aniversariantes = Meteor.users.find(
				search,
				{sort:{'profile.birthday':1}}
			);
			return aniversariantes;
		}
	}
});
