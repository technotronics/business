Meteor.publishComposite('oneConversa', function(id){
	return {
		find:function(){
			var conversa = Conversa.find(
				{
					_id:id,
					entidade_id:Meteor.settings.public.entidade_id
				}
			);
			return conversa;
		},
		children:[
			{
				find:function(conversa){
					var mensagens = Mensagem.find(
						{
							conversa_id:conversa._id,
							entidade_id:Meteor.settings.public.entidade_id
						}
					);
					return mensagens;
				},
				children:[
					{
						find:function(mensagem){
							return Meteor.users.find(
								{
									_id:mensagem.user_id,
									'profile.entidade_id':Meteor.settings.public.entidade_id
								}
							);
						}
					}
				]
			}
		]
	}
});

Meteor.publish('allConversas',function(){
  return Conversa.find({user_id:this.userId,entidade_id:Meteor.settings.public.entidade_id});
});
