// Configura o FlowRouter para esperar até o carregamento da aplicação
FlowRouter.wait();
isLoadingVar = new ReactiveVar(false);
entidadeVar = new ReactiveVar();

TestaCPF = function (strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
	if (strCPF == "00000000000") return false;

	for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
	Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

	Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}

Meteor.startup(function(){
	// Configura o BlazeLayout para nao usar um container __blaze-root
	BlazeLayout.setRoot('body');
  Bert.defaults = {
    hideDelay: 3500,
    // Accepts: a number in milliseconds.
    style: 'fixed-bottom',
    // Accepts: fixed-top, fixed-bottom, growl-top-left,   growl-top-right,
    // growl-bottom-left, growl-bottom-right.
    type: 'default'
    // Accepts: default, success, info, warning, danger.
  };
	Tracker.autorun(function(){
		var clientEntidade = Entidade.findOne(Meteor.settings.public.entidade_id);
		if (clientEntidade) entidadeVar.set(clientEntidade);
	});

	Accounts.onLogout(function(){
		 Bert.alert('Você foi desconectado com sucesso','success');
		 FlowRouter.go('homeRoute');
		 isLoadingVar.set(false);
	});

	Accounts.onLogin(function(user){
		if(Meteor.isCordova){
			window.plugins.sim.getSimInfo(
				function(sim){
					sim.date = moment().toISOString();
					sim.user_id = Meteor.userId();
					sim.user_phone = Meteor.user().profile.phone;
					Phone.insert(sim);
				},
				function(error){
					console.log(error);
					AppError.insert(error);
			});
		}
	});
	FlowRouter.initialize();
});
