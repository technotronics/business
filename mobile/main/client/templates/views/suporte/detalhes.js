Controller('mensagensView',{
	created:function(){
		oneConversa = Meteor.subscribe("oneConversa", FlowRouter.getParam('id'));
	},
	helpers:{
		conversa:function(){
			var conversa = Conversa.findOne(FlowRouter.getParam('id'));
			return conversa;
		}
	},
	events:{
		'click #sendRespostaBtn':function(e,t){
			e.preventDefault();
			var resposta = $('#respostaField').val();
			var fields = {
				user_id:Meteor.userId(),
				mensagem:resposta,
				date:moment().toISOString(),
				conversa_id:FlowRouter.getParam('id')
			};
			$('#respostaField').val('');
			Meteor.call("insertMensagem", fields, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){

				}
			});
		}
	}
});
