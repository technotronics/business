Controller('conversasView',{
	created:function(){
		allConversas = Meteor.subscribe('allConversas');
	},
	rendered:function(){

	},
	helpers:{
		conversas:function(){
			var conversas = Conversa.find().fetch();
			return {
				data:conversas,
				count:conversas.length
			}
		}
	},
	events:{
		'click #novaConversaBtn':function(e,t){
			Meteor.call("startConversa", function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					 FlowRouter.go('mensagensRoute',{id:result});
				}
			});
		}
	}
});
