Controller('galeriasView',{
	created:function(){
		subsGalerias = new SubsManager();
		Tracker.autorun(function(){
			allGalerias = subsGalerias.subscribe('allGalerias');
		});
	},
	rendered:function(){

	},
	helpers:{
		galerias:function(){
			var galerias = Galeria.find({},{sort:{'date':1}}).fetch();
			return galerias;
		}
	},
	events:{
	}
});
