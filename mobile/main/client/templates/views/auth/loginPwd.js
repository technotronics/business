Controller('loginPwdView',{
	created(){

	},
	rendered(){
		$('#usernameField').mask('999.999.999-99');
		$('#passwordField').mask('9999999999999999');
		$()
	},
	events:{
		'click .showPasswordBtn'(e,t){
			if ($(e.currentTarget).prev().hasClass('hidePwd')) {
				$(e.currentTarget).prev().removeClass('hidePwd');
			} else {
				$(e.currentTarget).prev().addClass('hidePwd');
			}
		},
		'submit #loginForm'(e,t){
			e.preventDefault();
			//isLoadingVar.set('Fazendo login...');
			$(e.currentTarget).addClass('loading');
			var fields = $(e.target).form('get values');
			//fields.username = fields.username.replace(/\-|\./g,'');
			Meteor.loginWithPassword(fields.username,fields.password,function(err,result){
				if(err){
					Bert.alert('Confira seu CPF e sua Senha!','danger');
					//isLoadingVar.set(false);
					$(e.currentTarget).removeClass('loading');
				} else {
					//isLoadingVar.set(false);
					$(e.currentTarget).removeClass('loading');
					FlowRouter.go('homeRoute');
					Bert.alert('Login efeutado com sucesso!','success');
				}
			});
		}
	}
});
