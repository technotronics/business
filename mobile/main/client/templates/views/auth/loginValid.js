Controller('loginPwdView',{
	rendered(){
		$('#loginForm').form({
			inline:true,
			fields:{
				emailField:{
					identifier:'emailField',
					rules:[
						{
							type:'email',
							prompt:'Email inválido'
						},
						{
							type:'empty',
							prompt:'Email em branco'
						}
					]
				},
				passwordField:{
					identifier:'passwordField',
					rules:[
						{
							type:'minLength[6]',
							prompt:'Senha deve ter 6 ou mais caracteres'
						}
					]
				}
			}
		});
	}
});
