Controller('loginView',{
	rendered:function(){
		console.log(Meteor.absoluteUrl());
	},
	events:{
		'click #loginWithFacebookBtn'(e,t){
			Meteor.loginWithFacebook({},function(error){
				console.log(error);
				if(!error){
					FlowRouter.go('homeRoute');
				}
			});
		},
		'click #loginWithGoogleBtn'(e,t){
			Meteor.loginWithGoogle({},function(error){
				if(!error){
					FlowRouter.go('homeRoute');
				} else {
					console.log(error);
				}
			});
		}
	}
})
