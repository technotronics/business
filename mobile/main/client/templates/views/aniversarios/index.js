Controller('aniversariosView',{
	created:function(){
		subsAniversariantes = new SubsManager();
		Tracker.autorun(function(){
			allAniversariantes = subsAniversariantes.subscribe('allAniversariantes');
		});
	},
	rendered:function(){

	},
	helpers:{
		subsReady:function(){
			return subsAniversariantes.ready();
		},
		aniversariantes:function(){
			var aniversariantes = Meteor.users.find().fetch();
			return aniversariantes;
		}
	},
	events:{
	}
});
