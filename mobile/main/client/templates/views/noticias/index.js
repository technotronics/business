Controller('noticiasView',{
	created:function(){
		subsNoticias = new SubsManager();
		Tracker.autorun(function(){
			pubNoticias = subsNoticias.subscribe('pubNoticias');
		});
	},
	rendered:function(){

	},
	helpers:{
		noticias:function(){
			var noticias = Noticia.find({},{sorte:{date:-1}});
			return {
				data: noticias.fetch(),
				count: noticias.count()
			};
		}
	},
	events:{
	}
});
