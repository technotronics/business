Controller('sidebar',{
	helpers:{
		userId:function(){
			return Meteor.userId();
		},
		isSuspended:function(){
			return Roles.userIsInRole(Meteor.userId(),'suspenso');
		}
	},
	events:{
		'click .item':function(){
			$('.ui.sidebar').sidebar('hide');
		}
	}
});
