Controller('adminLayout',{
  created(){
		var parser = document.createElement('a');
		parser.href = Meteor.absoluteUrl();
		subsEntidade = Meteor.subscribe('hostEntidade',parser.hostname);
		var hostEntidade = Entidade.findOne({
			hosts:parser.hostname
		});
		entidadeVar.set(hostEntidade);
  },
  rendered(){
		$('.ui.left.sidebar').sidebar('attach events', '.toggleSidebar');
		$('.ui.bottom.sidebar').sidebar('attach events', '.toggleTechnotronics');
  },
  events:{

  },
  helpers:{
		isOffline:function(){
			return !Meteor.status().connected;
		},
		isLoading:function(){
			return isLoadingVar.get();
		}
  }
});
