Controller('noticia.view', {
	created:function() {
		sint = 0;
		subsNoticias = new SubsManager();
		searchNoticiasVar = new ReactiveVar({});
		Tracker.autorun(function(){
			//subsNoticias.clear();
			allNoticias = subsNoticias.subscribe('allNoticias', searchNoticiasVar.get());
		});
	},
	rendered:function(){
		$('.toggle.checkbox').checkbox();
	},
	destroyed:function() {
	},
	helpers: {
		ready:function(){
			return subsNoticias.ready();
		},
		noticias: function() {
			var page = FlowRouter.getQueryParam('page');
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			var noticias = Noticia.find(searchNoticiasVar.get(),{sort:{date:-1},limit:qtd,skip:(page-1)*qtd}).fetch();

			$('.ui.progress').progress({
				duration	: 200,
				total			: Math.ceil(Counts.get('allNoticias')/qtd),
				value			: page
			});
			return {
				page: page,
				data: noticias,
				count: Counts.get('allNoticias'),
				pages: Math.ceil(Counts.get('allNoticias')/qtd)
			};
		}
	},
	events: {
		'keyup .titulo.search.item input.prompt':function(e,t){
			Meteor.clearTimeout(sint);
			sint = Meteor.setTimeout(function(){
				//subsNoticias.clear();
				var titulo = $('.titulo input.prompt').val();
				searchNoticiasVar.set({titulo:{$regex:titulo,$options: 'i'}});
			},1000);
		},
		'click .titulo.search.item .close.link.icon':function(e,t){
			e.preventDefault();
			subsNoticias.clear();
			$('.titulo.search.item input.prompt').val('');
			searchNoticiasVar.set({});
		},
		'click #addBtn':function(e,t){
			FlowRouter.go('noticiasInsertRoute');
		},
		'click .removeBtn':function(e,t){
			var me = this;
			htmlConfirm('Aviso','Você tem certeza?',function(){
				Meteor.call("removeNoticia", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						 Bert.alert('Notícia excluída com sucess','success','growl-top-right');
					}
				});
			});
		},
		'click #showMoreBtn':function(e,t){
			$('#showMoreBtn i').addClass('loading');
			$('#showMoreBtn span').html('Carregando...');
			var skip = FlowRouter.getQueryParam('skip');
			if (!skip) skip = 1;
			else skip++;
			FlowRouter.setQueryParams({'skip':skip});
		}
	}
});
