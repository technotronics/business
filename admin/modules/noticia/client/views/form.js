Controller('noticia.form',{
	rendered(){
		$('#noticiasForm .ui.dropdown').dropdown();
		$('#formNoticiasView').form({
			onFailure(prompts,values){
				return false;
			},
			inline:true,
			fields:{
				subjectField:{
					indentifier:'subjectField',
					rules:[
						{
							type:'empty',
							prompt:'O assunto não deve estar vazio'
						}
					]
				},
				bodyField:{
					indentifier:'bodyField',
					rules:[
						{
							type:'empty',
							prompt:'Escolha ao menos um destinatário'
						}
					]
				},
				toField:{
					indentifier:'toField',
					rules:[
						{
							type:'empty',
							prompt:'A mensagem não deve estar vazia'
						}
					]
				}
			}
		});
		if (id = FlowRouter.getParam('id')) {
			var noticia = Noticia.findOne(id);
			$('#noticiasForm').form('set values',noticia);
		}
		nicEditors.allTextAreas();
	},
	helpers:{
		user:function(){
			return Meteor.user();
		},
		destinatarios:function(){
			return Meteor.users.find().fetch();
		}
	},
	events:{
		'submit #noticiasForm'(e,t){
			e.preventDefault();
			var fields = $(e.target).form('get values');
			var id = FlowRouter.getParam('id');
			if (id) fields.id = id;
			Meteor.call("formNoticia",fields, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					 Bert.alert('A notícia foi enviada com sucesso!','success','growl-top-right');
					 FlowRouter.go('noticiasRoute')
				}
			});
		}
	}
});
