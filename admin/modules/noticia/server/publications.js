Meteor.publishComposite('allNoticias', function(search,page){
	return {
		find:function(){
			if (!search) search = {};
			if (!page) page = 1;
			var pages = Meteor.settings.public.paginator.itemsPerPage;
			Counts.publish(this,'allNoticias',Noticia.find(search), { noReady: true });
			var noticias = Noticia.find(search,{sort:{date:-1},limit:pages,skip:(page-1)*pages});
			return noticias;
		}
	}
});
