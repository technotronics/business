Meteor.methods({
	countNoticia(search){
		return Noticia.find(search).count();
	},
	formNoticia(fields) {
		if (!fields.id) {
			fields.user_id = this.userId;
			fields.date = moment().toISOString();
			Noticia.insert(fields);
		} else {
			Noticia.update(fields.id, {
				$set: fields
			});
		}
		return true;
	},
	removeNoticia(id) {
		return Noticia.remove(id);
	}
});
