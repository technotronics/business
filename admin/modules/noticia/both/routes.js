privateRoutes.route('/noticias',{
	name: 'noticia.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'noticia.view'
		});
	}
});

privateRoutes.route('/noticias/nova',{
	name: 'noticias.insert.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'noticia.form'
		});
	}
});

privateRoutes.route('/noticias/:id',{
	name: 'noticia.update.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'noticia.form'
		});
	}
});
