Meteor.publishComposite('oneConversa', function(id){
	return {
		find:function(){
			var conversa = Conversa.find(id);
			return conversa;
		},
		children:[
			{
				find:function(conversa){
					var mensagens = Mensagem.find({conversa_id:conversa._id});
					return mensagens;
				},
				children:[
					{
						find:function(mensagem){
							return Meteor.users.find(mensagem.user_id);
						}
					}
				]
			}
		]
	}
});
Meteor.publishComposite('allConversas', function(){
	return {
		find:function(){
			Counts.publish(this,'allConversas',Conversa.find());
			var conversas = Conversa.find();
			return conversas;
		},
		children:[
			{
				find:function(conversa){
					return Meteor.users.find({_id:conversa.user_id});
				}
			},
			{
				find:function(conversa){
					return Mensagem.find({conversa_id:conversa._id});
				}
			}
		]

	}
});
