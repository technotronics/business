Controller('conversasView', {
	created() {
		Tracker.autorun(function(){
			Meteor.subscribe("allConversas");
		});
	},
	rendered() {
	},
	helpers: {
		conversas: function() {
			var conversas = Conversa.find(
				{},
				{
					sort: {
						date: -1
					}
				}
			).fetch();
			return {
				data: conversas,
				count: Math.ceil(Counts.get('allConversas')/10)
			};
		}
	},
	events: {
		
	}
});
