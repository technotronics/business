Conversa = new Mongo.Collection('conversas');
Conversa.helpers({
  remetente:function(){
    return Meteor.users.findOne(this.user_id);
  },
	mensagens:function(){
		return Mensagem.find({conversa_id:this._id}).fetch();
	}
});

Mensagem = new Mongo.Collection('mensagens');
Mensagem.helpers({
  remetente:function(){
    return Meteor.users.findOne(this.user_id);
  }
});
