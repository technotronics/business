privateRoutes.route('/suporte',{
	name: 'suporte.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'suporte.view'
		});
	}

});

privateRoutes.route('/suporte/:id',{
	name: 'suporte.mensagem.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'suporte.mensagem.view'
		});
	}

});
