privateRoutes.route('/galerias',{
	name: 'galeria.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'galeria.view'
		});
	}

});

privateRoutes.route('/galerias/nova',{
	name: 'galeria.insert.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'galeria.form'
		});
	}
});

privateRoutes.route('/galerias/:id',{
	name: 'galeria.update.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'galeria.form'
		});
	}
});
