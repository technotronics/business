Galeria = new Mongo.Collection('galerias');
Galeria.helpers({
	fotos:function(){
		return Foto.find({galeria_id:this._id}).fetch();
	}
});
Foto = new Mongo.Collection('fotos');
Foto.helpers({
	galeria:function(){
		return Galeria.find(this.galeria_id);
	}
});
