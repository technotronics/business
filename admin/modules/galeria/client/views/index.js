Controller('galeriasView', {
	created:function() {
		subsGalerias = new SubsManager();
		Tracker.autorun(function(){
			allGalerias = subsGalerias.subscribe('allGalerias',FlowRouter.getQueryParam('page'));
		});
	},
	rendered:function(){
	},
	destroyed:function() {
	},
	helpers: {
		ready:function(){
			return subsGalerias.ready();
		},
		galerias: function() {
			if (!allGalerias.ready()) return false;
			var page = FlowRouter.getQueryParam('page');
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			var galerias = Galeria.find({},{sort:{date:-1},limit:qtd,skip:(page-1)*qtd}).fetch();

			$('.ui.progress').progress({
				duration	: 200,
				total			: Math.ceil(Counts.get('allGalerias')/qtd),
				value			: page
			});
			return {
				data: galerias,
				count: Counts.get('allGalerias'),
				pages: Math.ceil(Counts.get('allGalerias')/qtd)
			};
		}
	},
	events: {
		'click .editBtn':function(e,t){
			if ($(e.target).attr('type') == 'checkbox') return true;
			FlowRouter.go('galeriasUpdateRoute',{id:this._id});
		},
		'click .removeBtn':function(e,t){
			var me = this;
			htmlConfirm('Excluir Galerias','Você tem certeza ?',function(){
				Meteor.call("removeGaleria", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						 Bert.alert('Galeria(s) excluída(s) com sucesso.','success','growl-top-right');
					}
				});
			});
		}
	}
});
