Controller('formGaleriasView',{
	created:function(){
		subsGalerias = new SubsManager();
		oneGalerias = subsGalerias.subscribe('oneGalerias',FlowRouter.getParam('id'));
	},
	rendered(){
		$('#noticiasForm .ui.dropdown').dropdown();
		$('#formGaleriasView').form({
			onFailure(prompts,values){
				return false;
			},
			inline:true,
			fields:{
				subjectField:{
					indentifier:'subjectField',
					rules:[
						{
							type:'empty',
							prompt:'O título não deve estar vazio'
						}
					]
				}
			}
		});
		if (id = FlowRouter.getParam('id')) {
			var galeria = Galeria.findOne(id);
			$('#galeriasForm').form('set values',galeria);
		}
		var dropZone = $("div.dropzoneDiv").dropzone({
			url: location.origin+'/fotoUpload/'+FlowRouter.getParam('id'),
			dictDefaultMessage:'Clique ou arraste e solte as fotos aqui para enviar ao servidor.',
			dictInvalidFileType:'Apenas arquivos de imagems.',
			dictFileTooBig:'Arquivo muito grande',
			paramName: 'file',
			maxFilesize: 10,
			method:'post',
			acceptedFiles:'image/*'
		});
		dropZone.on('success',function(file){
		});
	},
	helpers:{
		galeria_id:function(){
			return FlowRouter.getParam('id');
		},
		fotos:function(){
			if (!FlowRouter.getParam('id')) return false;
			if (!subsGalerias.ready()) return false;
			var galeria = Galeria.findOne(FlowRouter.getParam('id'));
			return galeria.fotos();
		},
		capa_id:function(){
			if (!FlowRouter.getParam('id')) return false;
			if (!subsGalerias.ready()) return false;
			var galeria = Galeria.findOne(FlowRouter.getParam('id'));
			return galeria.capa_id;
		},
		galerias:function(){
			var galerias = Galeria.find();
			return {
				data:galerias.fetch(),
				total:1,
				pages:1
			}
		}
	},
	events:{
		'click .capaBtn'(e,t){
			var fields = {
				id:FlowRouter.getParam('id'),
				capa_id:this._id
			}
			Meteor.call("formGaleria", fields, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Foto marcada como capa com sucesso.','success','growl-top-right');
				}
			});
		},
		'click .deleteBtn'(e,t){
			var me = this;
			htmlConfirm('Excluir','Você tem certeza?',function(){
				Meteor.call("removeFoto", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						Bert.alert('Foto excluida com sucesso.','success','growl-top-right');
					}
				});
			});
		},
		'submit #galeriasForm'(e,t){
			e.preventDefault();
			var fields = $(e.target).form('get values');
			var id = FlowRouter.getParam('id');
			if (id) fields.id = id;
			Meteor.call("formGaleria",fields, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					 Bert.alert('A galeria foi salva com sucesso!','success','growl-top-right');
					 FlowRouter.go('galeriasRoute')
				}
			});
		}
	}
});
