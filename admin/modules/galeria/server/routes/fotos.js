Picker.route('/galerias/foto/:foto_id', function(params, req, res, next) {
	try {
		var foto = Foto.findOne(params.foto_id);
		res.writeHead(200, {'Content-Type': 'image' });
		gm(foto.path).autoOrient().resize(320).stream(function (err, stdout, stderr) {
      stdout.pipe(res)
    });
	} catch(err) {
		res.end();
	}
});
