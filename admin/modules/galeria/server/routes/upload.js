var bodyParser = require('body-parser');
var multer = require('multer');
var multerMid = multer({dest: '/upload/galerias/fotos'});
Picker.middleware(multerMid.any());

Picker.route('/galerias/:galeria_id/upload', function(params, req, res, next) {
	_.each(req.files,function(uploaded){
		uploaded.galeria_id = params.galeria_id;
		Foto.insert(uploaded);
	});
	res.end();
});
