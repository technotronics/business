Meteor.publishComposite('allGalerias', function(page){
	return {
		find:function(){
			if (!page) page = 1;
			Counts.publish(this,'allGalerias',Galeria.find({removed:false}));
			var galerias = Galeria.find({removed:false},{sort:{date:-1},limit:10,skip:(page-1)*10});
			return galerias;
		},
		children:[
			{
				find:function(galeria){
					return Foto.find({galeria_id:galeria._id});
				}
			}
		]
	}
});

Meteor.publishComposite('oneGalerias', function(id){
	return {
		find:function(){
			var galeria = Galeria.find(id);
			return galeria;
		},
		children:[
			{
				find:function(galeria){
					return Foto.find({galeria_id:galeria._id});
				}
			}
		]
	}
});
