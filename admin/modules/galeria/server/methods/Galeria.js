Meteor.methods({
	formGaleria(fields) {
		if (!fields.id) {
			fields.user_id = this.userId;
			fields.date = moment().toISOString();
			fields.removed = false;
			Galeria.insert(fields);
		} else {
			Galeria.update(fields.id, {
				$set: fields
			});
		}
		return true;
	},
	removeGaleria(id) {
		return Galeria.update(id,{$set:{removed:true}});
	}
});
