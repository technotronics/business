Meteor.methods({
	removeFoto(id) {
		const fs = require('fs');
		let foto = Foto.findOne(id);
		fs.unlinkSync(foto.path);
		return Foto.remove(id);
	}
});
