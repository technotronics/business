var bodyParser = require('body-parser');
var multer = require('multer');
var multerMid = multer({dest: '/upload/escalas/pdf'});
Picker.middleware(multerMid.any());

Picker.route('/escalas/:galeria_id/upload', function(params, req, res, next) {
	_.each(req.files,function(uploaded){
		uploaded.galeria_id = params.galeria_id;
		Foto.insert(uploaded);
	});
	res.end();
});
