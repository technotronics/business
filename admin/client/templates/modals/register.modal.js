Controller('register.modal',{
  created:function(){
  },
  rendered:function(){
    $('#registerModal').modal({
			detachable:false,
			autofocus:false,
			transition:'horizontal flip'
    });
  },
  events:{
		'click #loginEvent':function(e,t){
			$('#loginModal').modal('show');
		},
    'click #loginWithGoogle':function(e,t){
      Meteor.loginWithGoogle(function(erro,result){
				  $('#registerModal').modal('hide');
      });
    }
  }
});
