Controller('login.modal',{
  created:function(){
		aceiteVar = new ReactiveVar(true);
  },
  rendered:function(){
    $('#loginModal').modal({
      detachable:false,
			autofocus:false,
			transition:'horizontal flip'
    });
		$('.ui.checkbox').checkbox();
  },
	helpers:{
		aceite:function(){
			return aceiteVar.get();
		}
	},
  events:{
		'change #aceiteChk':function(){
			var aceite = $('#aceiteChk').checkbox('is checked');
			aceiteVar.set(aceite);
		},
		'click #registerEvent':function(e,t){
			//$('#loginModal').modal('hide');
			$('#registerModal').modal('show');
		},
    'click #loginWithGoogle':function(e,t){
      Meteor.loginWithGoogle(function(error,result){
				$('#loginModal').modal('hide');
				FlowRouter.go('home.route');
				Bert.alert('Login efeutado com sucesso!','success');
      });
    },
    'submit #login-form':function(e,t){
      e.preventDefault();
			var fields = $(e.target).form('get values');
			$(e.target).addClass('loading');
			Meteor.loginWithPassword(fields.username,fields.password,function(err,result){
				if(err){
					$(e.target).removeClass('loading');
					Bert.alert('Email ou senha inválidos!','danger');
				} else {
					$(e.target).removeClass('loading');
					$('#loginModal').modal('hide');
					FlowRouter.go('home.route');
					Bert.alert('Login efeutado com sucesso!','success');
				}
			});
    }
  }
});
