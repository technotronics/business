Controller('main.menu',{
	rendered(){
		$('.ui.sticky').sticky();
	},
	helpers:{
		userId(){
			return Meteor.userId();
		},
		userEmail(){
			if (!Meteor.user()) return '';
			return Meteor.user().emails[0].address;
		}
	},
	events:{
		'click #loginEvent':function(e,t){
			$('#loginModal').modal('show');
		},
		'click #registerEvent':function(e,t){
			$('#registerModal').modal('show');
		},
		'click #logoutEvent'(e,t){
			htmlConfirm('Sair do Sistema','Você tem certeza?',function(){
				isLoadingVar.set(true);
				Meteor.logout(function(){
					Bert.alert('Usuário desconectado com sucesso','success');
					FlowRouter.go('home.route');
					isLoadingVar.set(false);
				});
			});
		}
	}
});
