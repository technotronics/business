Template.registerHelper("currency", function(number){
  number = parseFloat(number);
  return number.toFixed(2);
});
