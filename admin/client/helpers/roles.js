Template.registerHelper("hasRole", function(userId, role, group){
	if (userId == 'logged') {
		userId = Meteor.userId();
	}
  return Roles.userIsInRole(userId, role, group);
});

Template.registerHelper("getRolesForUser", function(userId, group){
  return Roles.getRolesForUser(userId,group)[0];
});
