// Configura o FlowRouter para esperar até o carregamento da aplicação
FlowRouter.wait();
Meteor.startup(function(){
	// Configura o BlazeLayout para nao usar um container __blaze-root
	BlazeLayout.setRoot('body');

	// Configura o comportamento padrão do Bert.Alert
	Bert.defaults = {
		hideDelay: 3500,
		style: 'growl-top-right',
		type: 'default'
	};

	$.fn.form.settings.rules.validCPF = function(value) {
		return TestaCPF(value);
	};

	Meteor.autorun(function(){
		// Libera o router após carregar as Roles
		if (Roles.subscription.ready()) {
			FlowRouter.initialize();
		}
	});
});
// Gerar APK pela interface web com controle da versao, fazer download do APK para o proprio cliente colocar no google play
// Sistema de Pagamento online para os cliente
// Plataforma de desenvolvimento de Aplicativos automatizado
// AppCreator - AppMachine
