privateRoutes.route('/entidades',{
	name: 'entidade.view.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'entidade.view'
		});
	}
});

privateRoutes.route('/entidades/configure/:id',{
	name: 'entidade.configure.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'entidade.configure.modulo'
		});
	}
});

privateRoutes.route('/entidades/configure/usuario/:id',{
	name: 'entidade.configure.usuario.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'entidade.configure.usuario'
		});
	}
});

privateRoutes.route('/entidades/nova',{
	name: 'entidade.insert.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'entidade.form'
		});
	}
});

privateRoutes.route('/entidades/:id',{
	name: 'entidade.update.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'entidade.form'
		});
	}
});
