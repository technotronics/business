Entidade = new Mongo.Collection('entidades');
Entidade.helpers({
	modulos:function(){
		return EntidadeModulo.find({entidade_id:this._id}).fetch();
	},
	usuarios:function(){
		return EntidadeUser.find({user_id:this._id}).fetch();
	}
});
