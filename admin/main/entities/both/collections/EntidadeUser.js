EntidadeUser = new Mongo.Collection('entidades_users');
EntidadeUser.helpers({
	user:function(){
		return Meteor.users.findOne(this.user_id);
	},
	entidade:function(){
		return Entidade.findOne(this.entidade_id);
	}
});
