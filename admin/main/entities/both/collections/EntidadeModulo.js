EntidadeModulo = new Mongo.Collection('entidades_modulos');
EntidadeModulo.helpers({
	modulo:function(){
		return Modulo.findOne(this.modulo_id);
	},
	entidade:function(){
		return Entidade.findOne(this.entidade_id);
	}
});
