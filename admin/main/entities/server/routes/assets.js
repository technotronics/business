Picker.route('/entidades/:type/:entidade_id', function(params, req, res, next) {
	var type = params.type;
	if (type == 'logotipo') {
		try {
			var ent = Entidade.findOne(params.entidade_id);
			res.writeHead(200, {'Content-Type': 'image' });
			gm(ent.logo.path).autoOrient().resize(320).stream(function (err, stdout, stderr) {
	      stdout.pipe(res)
	    });
		} catch(err) {
			res.end();
		}
	}
	res.end();
});
