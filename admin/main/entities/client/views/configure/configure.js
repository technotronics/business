Controller('entidade.configure.modulo',{
	created:function(){
		subsEntidades = new SubsManager();
		subsModulos = new SubsManager();
		Tracker.autorun(function(){
			search = {active:true};
			oneEntidade = subsEntidades.subscribe('one-entidade', FlowRouter.getParam('id'));
			availModulos = subsModulos.subscribe('availModulos', search);
		});
	},
	rendered:function(){
		$('.ui.dropdown#selectModulo').dropdown({
			onChange:function(value,title){
				Meteor.call("entidade.modulo.insert", FlowRouter.getParam('id'), value, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						Bert.alert('Módulo incluído na Entidade com sucesso.','success');
					}
				});
			}
		});
	},
	helpers:{
		entidade:function(){
			var entidade = Entidade.findOne(FlowRouter.getParam('id'));
			return entidade;
		},
		modulos_disponiveis:function(){
			if (!subsEntidades.ready()) return false;
			var entidade = Entidade.findOne(FlowRouter.getParam('id'));
			var entMods = entidade.modulos();
			var modulo_ids = [];
			_.each(entMods,function(mod){
				modulo_ids.push(mod.modulo_id);
			});
			var modulos = Modulo.find({active:true,_id:{$not:{$in:modulo_ids}}},{sort:{nome:1}}).fetch();
			return modulos;
		},
		modulos:function(){
			if (!oneEntidade.ready()) return false;
			var entidade = Entidade.findOne(FlowRouter.getParam('id'));
			return entidade.modulos();
		}
	},
	events:{
		'click #removeEvent':function(e,t){
			var me = this;
			htmlConfirm('Remover Módulo','Você tem certeza?',function(){
				Meteor.call("entidade.modulo.remove", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						 Bert.alert('Módulo removido com sucesso.','success');
					}
				});
			});
		},
		'click #activateEvent':function(e,t){
			Meteor.call("entidade.modulo.activate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Módulo ativado com sucesso.','success');
				}
			});
		},
		'click #deactivateEvent':function(e,t){
			Meteor.call("entidade.modulo.deactivate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Módulo desativado com sucesso.','success');
				}
			});
		}
	}
});
