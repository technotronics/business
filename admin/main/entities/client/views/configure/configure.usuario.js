Controller('entidade.configure.usuario',{
	created:function(){
		subsEntidades = new SubsManager();
		subsUsuarios = new SubsManager();
		Tracker.autorun(function(){
			search = {active:true};
			oneEntidade = subsEntidades.subscribe('one-entidade', FlowRouter.getParam('id'));
			availUsuarios = subsUsuarios.subscribe('availUsuarios', search);
		});
	},
	rendered:function(){
		$('.ui.dropdown#selectUsuario').dropdown({
			onChange:function(value,title){
				Meteor.call("entidade.usuario.insert", FlowRouter.getParam('id'), value, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						Bert.alert('Usuário incluído na Entidade com sucesso.','success');
					}
				});
			}
		});
	},
	helpers:{
		usuarios_disponiveis:function(){
			if (!subsEntidades.ready()) return false;
			var entidade = Entidade.findOne(FlowRouter.getParam('id'));
			var entUsers = entidade.usuarios();
			var usuarios_ids = [];
			_.each(entUsers,function(user){
				usuarios_ids.push(user.usuario_id);
			});
			var usuarios = Meteor.users.find({active:true,_id:{$not:{$in:usuarios_ids}}},{sort:{'profile.name':1}}).fetch();
			return usuarios;
		},
		usuarios:function(){
			if (!oneEntidade.ready()) return false;
			var entidade = Entidade.findOne(FlowRouter.getParam('id'));
			return entidade.usuarios();
		}
	},
	events:{
		'click #removeEvent':function(e,t){
			var me = this;
			htmlConfirm('Remover Usuário','Você tem certeza?',function(){
				Meteor.call("entidade.usuario.remove", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						 Bert.alert('Usuário removido com sucesso.','success');
					}
				});
			});
		},
		'click #activateEvent':function(e,t){
			Meteor.call("entidade.usuario.activate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Usuário ativado com sucesso.','success');
				}
			});
		},
		'click #deactivateEvent':function(e,t){
			Meteor.call("entidade.usuario.deactivate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Usuário desativado com sucesso.','success');
				}
			});
		}
	}
});
