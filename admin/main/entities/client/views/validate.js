Controller('entidade.form',{
	rendered:function(){
		$('#entidade-form').form({
			onFailure(prompts,values){
				return false;
			},
			inline:true,
			fields:{
				nomeField:{
					indentifier:'nome',
					rules:[
						{
							type:'empty',
							prompt:'O nome não deve estar vazio'
						}
					]
				},
				hostnameField:{
					indentifier:'hostname',
					rules:[
						{
							type:'empty',
							prompt:'O Hostname não deve estar vazio'
						}
					]
				},
				logotipoField:{
					indentifier:'logotipo',
					rules:[
						{
							type:'empty',
							prompt:'O logotipo não deve estar vazio'
						}
					]
				}
			}
		});
	}
});
