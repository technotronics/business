Controller('entidade.view', {
	created:function() {
		sint = 0;
		subsEntidades = new SubsManager();
		searchEntidadesVar = new ReactiveVar({});
		Tracker.autorun(function(){
			allEntidades = subsEntidades.subscribe('all-entidade', searchEntidadesVar.get());
		});
	},
	rendered:function(){
	},
	destroyed:function() {
	},
	helpers: {
		ready:function(){
			return subsEntidades.ready();
		},
		entidades: function() {
			var page = FlowRouter.getQueryParam('page');
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			var entidades = Entidade.find(searchEntidadesVar.get(),{sort:{date:-1},limit:qtd,skip:(page-1)*qtd}).fetch();

			$('.ui.progress').progress({
				duration	: 200,
				total			: Math.ceil(Counts.get('all-entidade')/qtd),
				value			: page
			});
			return {
				page: page,
				data: entidades,
				count: Counts.get('all-entidade'),
				pages: Math.ceil(Counts.get('all-entidade')/qtd)
			};
		}
	},
	events: {
		'keyup .nome.search.item input.prompt':function(e,t){
			Meteor.clearTimeout(sint);
			sint = Meteor.setTimeout(function(){
				var titulo = $('.nome input.prompt').val();
				searchEntidadesVar.set({titulo:{$regex:nome,$options: 'i'}});
			},1000);
		},
		'click .nome.search.item .close.link.icon':function(e,t){
			e.preventDefault();
			subsEntidades.clear();
			$('.nome.search.item input.prompt').val('');
			searchEntidadesVar.set({});
		},
		'click #activateEvent':function(e,t){
			Meteor.call("entidade.activate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Entidade ativada com sucesso.','success');
				}
			});
		},
		'click #deactivateEvent':function(e,t){
			Meteor.call("entidade.deactivate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Entidade desativada com sucesso.','success');
				}
			});
		},
		'click #addBtn':function(e,t){
			FlowRouter.go('entidade.insert.route');
		},
		'click .removeBtn':function(e,t){
			var me = this;
			htmlConfirm('Aviso','Você tem certeza?',function(){
				Meteor.call("entidade.remove", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						 Bert.alert('Entidade excluída com sucess','success','growl-top-right');
					}
				});
			});
		}
	}
});
