Controller('entidade.form',{
	created:function(){
		subsEntidades = new SubsManager();
		Tracker.autorun(function(){
			oneEntidade = subsEntidades.subscribe('one-entidade', FlowRouter.getParam('id'));
		});
	},
	rendered(){
		$('#entidade-form .ui.dropdown').dropdown();
		if (id = FlowRouter.getParam('id')) {
			var entidade = Entidade.findOne(id);
			$('#entidade-form').form('set values',entidade);
		}
		nicEditors.allTextAreas();
	},
	helpers:{
		user:function(){
			return Meteor.user();
		}
	},
	events:{
		'submit #entidade-form'(e,t){
			e.preventDefault();
			var fields = $(e.target).form('get values');
			var id = FlowRouter.getParam('id');
			if (id) fields.id = id;
			Meteor.call("entidade.save",fields, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					 Bert.alert('A entidade foi gravada com sucesso!','success','growl-top-right');
					 FlowRouter.go('entidade.view.route')
				}
			});
		}
	}
});
