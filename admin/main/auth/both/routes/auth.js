publicRoutes.route('/login',{
	name: 'login.social.route',
	action: function() {
		BlazeLayout.render('login.layout',{
			main:'login.social.view'
		});
	}
});

publicRoutes.route('/login-password',{
	name: 'login.password.route',
	action: function() {
		BlazeLayout.render('login.layout',{
			main:'login.password.view'
		});
	}
});
publicRoutes.route('/cadastro',{
	name: 'register.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			main:'register.view'
		});
	}
});

publicRoutes.route('/password',{
	name: 'password.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			main:'password.view'
		});
	}
});

publicRoutes.route('/logout',{
	name: 'logout.route',
	triggersEnter:[
		function(){
			Meteor.logout();
		}
	]
});
