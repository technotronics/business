Meteor.methods({
	'formUsuario'(usuario){
		if (usuario.id == undefined) {
			var id = Accounts.createUser(usuario);
			Roles.addUsersToRoles(id, usuario.tipo, 'user-type');
			Roles.addUsersToRoles(id, 'Novo', 'user-status');
			return id;
		} else {
			Meteor.users.update(usuario.id,{$set:usuario});
			Roles.removeUsersFromRoles(usuario.id, [], 'user-type');
			Roles.addUsersToRoles(usuario.id, usuario.tipo, 'user-type');
			return true;
		}
	},
	'removeUsuario'(id){
		Meteor.users.remove(id);
		return true;
	},
	'findUserByEmail'(email){
		return Accounts.findUserByEmail(email);
	},
	suspendUser:function(id){
		Roles.removeUsersFromRoles(id,[],'user-status');
		Roles.addUsersToRoles(id,'Suspenso','user-status');
		return true;
	},
	activateUser:function(id){
		Roles.removeUsersFromRoles(id,[],'user-status');
		Roles.addUsersToRoles(id,'Ativo','user-status');
		return true;
	}
})
