Controller('register.view',{
	rendered(){
		$('#register-form').form({
			inline:true,
			fields:{
				usernameField:{
					identifier:'username',
					rules:[
						{
							type:'empty',
							prompt:'Digite seu nome de usuário.'
						}
					]
				},
				password1Field:{
					identifier:'password1',
					rules:[
						{
							type:'minLength[6]',
							prompt:'Senha deve ter 6 ou mais caracteres.'
						}
					]
				},
				password2Field:{
					identifier:'password2',
					rules:[
						{
							type:'match[password1Field]',
							prompt:'As senhas digitadas não são iguais.'
						}
					]
				}
			}
		});
	}
});
