Controller('login.password.view',{
	rendered(){
	},
	events:{
		'submit #login-form'(e,t){
			e.preventDefault();
			isLoadingVar.set(true);
			var fields = $(e.target).form('get values');
			Meteor.loginWithPassword(fields.username,fields.password,function(err,result){
				if(err){
					isLoadingVar.set(false);
					Bert.alert('Confira seu Usuário e sua Senha!','danger');
				} else {
					isLoadingVar.set(false);
					FlowRouter.go('home.route');
					Bert.alert('Login efeutado com sucesso!','success');
				}
			});
		}
	}
});
