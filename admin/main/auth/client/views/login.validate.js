Controller('login.password.view',{
	rendered(){
		$('#login-form').form({
			inline:true,
			fields:{
				usernameField:{
					identifier:'username',
					rules:[
						{
							type:'empty',
							prompt:'Digite seu nome de usuário.'
						}
					]
				},
				passwordField:{
					identifier:'password',
					rules:[
						{
							type:'minLength[6]',
							prompt:'Senha deve ter 6 ou mais caracteres.'
						}
					]
				}
			}
		});
	}
});
