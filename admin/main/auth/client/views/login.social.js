Controller('login.social.view',{
	events:{
		'click #loginGoogleEvent'(e,t){
			Meteor.loginWithGoogle({},function(error){
				if(!error){
					FlowRouter.go('home.route');
				} else {
					Bert.alert('Houve um erro ao fazer o Login pelo Google','error');
				}
			});
		}
	}
})
