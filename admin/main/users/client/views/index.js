Controller('usuariosView',{
	created(){
		subsUsers = new SubsManager();
		searchUsuariosVar = new ReactiveVar({});
		Meteor.autorun(function(){
			var page = FlowRouter.getQueryParam('page');
			if (!page) page = 1;
			var allUsuarios = subsUsers.subscribe('allUsuarios',page);
		});
	},
	helpers:{
		ready:function(){
			return subsUsers.ready();
		},
		usuarios:function(){
			var page = FlowRouter.getQueryParam('page');
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			var usuarios = Meteor.users.find(searchUsuariosVar.get(),{sort:{'profile.name':1},limit:qtd,skip:(page-1)*qtd});

			$('.ui.progress').progress({
				duration	: 200,
				total			: Math.ceil(Counts.get('allUsuarios')/qtd),
				value			: page
			});
			return {
				page:page,
				data:usuarios.fetch(),
				pages: Math.ceil(Counts.get('allUsuarios')/qtd),
				count:Counts.get('allUsuarios')
			}
		}
	},
	events:{
		'click #addBtn':function(){
			FlowRouter.go('usuariosInsertRoute');
		},
		'click #activateUserBtn':function(e,t){
			var me = this;
			var id = this._id;
			if (id == Meteor.userId()) {
				Bert.alert('Você não pode ativar a sí mesmo.','danger','growl-top-right');
				return false;
			} else {
				Meteor.call("activateUser", id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						Bert.alert('Usuário ativado com sucesso.','success','growl-top-right');
						return true;
					}
				});
			}
		},
		'click #suspendUserBtn':function(e,t){
			var me = this;
			var id = this._id;
			if (id == Meteor.userId()) {
				Bert.alert('Você não pode suspender a sí mesmo.','danger','growl-top-right');
				return false;
			} else {
				Meteor.call("suspendUser", id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						Bert.alert('Usuário suspendido com sucesso.','success','growl-top-right');
						return true;
					}
				});
			}
		},
		'click .removeBtn':function(e,t){
			var me = this;
			htmlConfirm('Excluir Usuário','Você tem certeza?',function(){
				Meteor.call('removeUsuario',me._id,function(err, result){
					Bert.alert('Usuário removido com sucesso!','success','growl-top-right');
				});
			});
		}
	}
});
