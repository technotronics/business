privateRoutes.route('/usuarios',{
	name: 'user.view.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'user.view'
		});
	}
});

privateRoutes.route('/usuarios/novo',{
	name: 'user.insert.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'user.form'
		});
	}
});

privateRoutes.route('/usuarios/:id',{
	name: 'user.update.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'user.form'
		});
	}
});
