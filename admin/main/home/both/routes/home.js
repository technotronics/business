publicRoutes.route('/',{
	name: 'home.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'home.view'
		});
	}
});

FlowRouter.notFound = {
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'notfound.view'
		});
  }
};
