Controller('modulo.view', {
	created:function() {
		sint = 0;
		subsModulos = new SubsManager();
		searchModulosVar = new ReactiveVar({});
		Tracker.autorun(function(){
			var search = searchModulosVar.get();
			allModulos = subsModulos.subscribe('allModulos', search);
		});
	},
	rendered:function(){
	},
	destroyed:function() {
	},
	helpers: {
		ready:function(){
			return subsModulos.ready();
		},
		modulos: function() {
			var page = FlowRouter.getQueryParam('page');
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			var modulos = Modulo.find(searchModulosVar.get(),{sort:{nome:1},limit:qtd,skip:(page-1)*qtd}).fetch();

			$('.ui.progress').progress({
				duration	: 200,
				total			: Math.ceil(Counts.get('allModulos')/qtd),
				value			: page
			});
			return {
				page: page,
				data: modulos,
				count: Counts.get('allModulos'),
				pages: Math.ceil(Counts.get('allModulos')/qtd)
			};
		}
	},
	events: {
		'keyup .nome.search.item input.prompt':function(e,t){
			Meteor.clearTimeout(sint);
			sint = Meteor.setTimeout(function(){
				var nome = $('.nome input.prompt').val();
				searchModulosVar.set({nome:{$regex:nome,$options: 'i'}});
			},1000);
		},
		'click .nome.search.item .close.link.icon':function(e,t){
			e.preventDefault();
			subsModulos.clear();
			$('.nome.search.item input.prompt').val('');
			searchModulosVar.set({});
		},
		'click #activateEvent':function(e,t){
			Meteor.call("modulo.activate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Módulo ativado com sucesso.','success');
				}
			});
		},
		'click #deactivateEvent':function(e,t){
			Meteor.call("modulo.deactivate", this._id, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					Bert.alert('Módulo desativado com sucesso.','success');
				}
			});
		},
		'click #addBtn':function(e,t){
			FlowRouter.go('modulo.insert.route');
		},
		'click .removeBtn':function(e,t){
			var me = this;
			htmlConfirm('Aviso','Você tem certeza?',function(){
				Meteor.call("modulo.remove", me._id, function(error, result){
					if(error){
						console.log("error", error);
					}
					if(result){
						 Bert.alert('Módulo excluído com sucesso','success');
					}
				});
			});
		}
	}
});
