Controller('modulo.form',{
	rendered(){
		$('#moduloForm').form({
			onFailure(prompts,values){
				return false;
			},
			inline:true,
			fields:{
				nomeField:{
					indentifier:'nomeField',
					rules:[
						{
							type:'empty',
							prompt:'O nome não deve estar vazio'
						}
					]
				}
			}
		});
	}
});
