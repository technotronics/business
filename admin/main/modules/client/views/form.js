Controller('modulo.form',{
	rendered(){
		$('#moduloForm .ui.dropdown').dropdown();
		if (id = FlowRouter.getParam('id')) {
			var modulo = Modulo.findOne(id);
			$('#moduloForm').form('set values',modulo);
		}
		nicEditors.allTextAreas();
	},
	helpers:{
	},
	events:{
		'submit #moduloForm'(e,t){
			e.preventDefault();
			var fields = $(e.target).form('get values');
			var id = FlowRouter.getParam('id');
			if (id) fields.id = id;
			Meteor.call("modulo.save",fields, function(error, result){
				if(error){
					console.log("error", error);
				}
				if(result){
					 Bert.alert('O Módulo foi gravado com sucesso!','success');
					 FlowRouter.go('modulo.view.route')
				}
			});
		}
	}
});
