privateRoutes.route('/modulos',{
	name: 'modulo.view.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'modulo.view'
		});
	}
});

privateRoutes.route('/modulos/novo',{
	name: 'modulo.insert.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'modulo.form'
		});
	}
});

privateRoutes.route('/modulos/:id',{
	name: 'modulo.update.route',
	action: function() {
		BlazeLayout.render('admin.layout',{
			menu:'main.menu',
			main:'modulo.form'
		});
	}
});
