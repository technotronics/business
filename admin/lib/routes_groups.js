publicRoutes = FlowRouter.group({
	name:'publicRoutes'
});

privateRoutes = FlowRouter.group({
	name:'privateRoutes',
	triggersEnter:[
		function(obj,redirect){
			if (!Meteor.userId()){
				Bert.alert('Você precisa fazer o login para ter permissão de acesso.','warning');
				redirect('login.route');
			}
		}
	]
});
