Meteor.startup(function(){
	var admins = Roles.getUsersInRole('Super Administrador','user-type');
	if (admins.count()==0){
		var admin = {
			email:'admin@technotronics.com.br',
			password:'123123',
			profile:{
				name:'Administrador'
			}
		};
		var id = Accounts.createUser(admin);
		Roles.addUsersToRoles(id,['Super Administrador'],'user-type');
		Roles.addUsersToRoles(id,['Ativo'],'user-status');
		console.log('Usuário Admin criado!');
	}
	var entidades = Entidade.find().count();
	if (entidades == 0) {
		var entidade = {
			nome:'Techno Business',
			ativa:"1",
			hosts:[
				'localhost',
				'business.technotronics.com.br'
			]
		};
		Entidade.insert(entidade);
	}
});
