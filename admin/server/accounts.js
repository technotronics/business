Meteor.startup(function(){
  Accounts.onLogin(function(user){
     return true;
  });
	Accounts.onCreateUser(function(options, user){
		Roles.addUsersToRoles(user._id,['Entidade'],'user-type');
		Roles.addUsersToRoles(user._id,['Novo'],'user-status');
		return user;
	});
});
