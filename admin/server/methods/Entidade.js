Meteor.methods({
	'entidade.save':function(fields) {
		if (!fields.id) {
			fields.user_created = this.userId;
			fields.date_created = moment().toISOString();
			fields.active = false;
			fields.removed = false;
			Entidade.insert(fields);
		} else {
			fields.user_updated = this.userId;
			fields.date_updated = moment().toISOString();
			Entidade.update(fields.id, {
				$set: fields
			});
		}
		return true;
	},
	'entidade.remove':function(id) {
		var softRemove = Meteor.settings.softRemove;
		if (softRemove) {
			Entidade.update(id, {
				$set: {removed:true}
			});
		} else {
			Entidade.remove(id);
		}
		return true;
	},
	'entidade.unremove':function(id) {
		Entidade.update(id, {
			$set: {removed:false}
		});
		return true;
	},
	'entidade.activate':function(id) {
		Entidade.update(id, {
			$set: {active:true}
		});
		return true;
	},
	'entidade.deactivate':function(id) {
		Entidade.update(id, {
			$set: {active:false}
		});
		return true;
	},
	'entidade.modulo.insert':function(id,modulo_id){
		var entMod = {
			active:false,
			modulo_id:modulo_id,
			entidade_id:id
		};
		EntidadeModulo.insert(entMod);
		return true;
	},
	'entidade.modulo.activate':function(id) {
		EntidadeModulo.update(id, {
			$set: {active:true}
		});
		return true;
	},
	'entidade.modulo.deactivate':function(id) {
		EntidadeModulo.update(id, {
			$set: {active:false}
		});
		return true;
	},
	'entidade.modulo.remove':function(id) {
		EntidadeModulo.remove(id);
		return true;
	}
});
