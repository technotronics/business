Meteor.methods({
	'modulo.save':function(fields) {
		if (!fields.id) {
			fields.user_created = this.userId;
			fields.date_created = moment().toISOString();
			fields.active = false;
			fields.removed = false;
			Modulo.insert(fields);
		} else {
			fields.user_updated = this.userId;
			fields.date_updated = moment().toISOString();
			Modulo.update(fields.id, {
				$set: fields
			});
		}
		return true;
	},
	'modulo.remove':function(id) {
		var softRemove = Meteor.settings.softRemove;
		if (softRemove) {
			Modulo.update(id, {
				$set: {removed:true}
			});
		} else {
			Modulo.remove(id);
		}
		return true;
	},
	'modulo.unremove':function(id) {
		Modulo.update(id, {
			$set: {removed:false}
		});
		return true;
	},
	'modulo.activate':function(id) {
		Modulo.update(id, {
			$set: {active:true}
		});
		return true;
	},
	'modulo.deactivate':function(id) {
		Modulo.update(id, {
			$set: {active:false}
		});
		return true;
	}
});
