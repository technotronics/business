Meteor.publishComposite('allUsuarios', function(page,role){
	return {
		find:function(){
			if (!page) page = 1;
			if(role) {
				typeSearch = {
					role:[role]
				}
			} else {
				typeSearch = {};
			}
			Counts.publish(this,'allUsuarios',Meteor.users.find());
			return Meteor.users.find({},{
				skip:(page-1)*10,
				limit: 10,
				sort:{
					'profile.name':1
				}
			});
		},
		children:[
		]
	}
});

Meteor.publishComposite('availUsuarios', function(search,page){
	return {
		find:function(){
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			if (!search) search = {};
			search.active = true;
			var softRemove = Meteor.settings.softRemove;
			if (softRemove) {
				search.removed = false;
			}

			Counts.publish(this,'availUsuarios',Meteor.users.find(search), { noReady: true });
			var usuarios = Meteor.users.find(search,{sort:{'profile.name':1},limit:qtd,skip:(page-1)*qtd});
			return usuarios;
		}
	}
});
