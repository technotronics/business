Meteor.publishComposite('allModulos', function(search,page){
	return {
		find:function(){
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			if (!search) search = {};
			var softRemove = Meteor.settings.softRemove;
			if (softRemove) {
				search.removed = false;
			}

			Counts.publish(this,'allModulos',Modulo.find(search), { noReady: true });
			var modulos = Modulo.find(search,{sort:{nome:1},limit:qtd,skip:(page-1)*qtd});
			return modulos;
		}
	}
});
Meteor.publishComposite('availModulos', function(search,page){
	return {
		find:function(){
			if (!page) page = 1;
			var qtd = Meteor.settings.public.paginator.itemsPerPage;
			if (!search) search = {};
			search.active = true;
			var softRemove = Meteor.settings.softRemove;
			if (softRemove) {
				search.removed = false;
			}

			Counts.publish(this,'availModulos',Modulo.find(search), { noReady: true });
			var modulos = Modulo.find(search,{sort:{nome:1},limit:qtd,skip:(page-1)*qtd});
			return modulos;
		}
	}
});
