Meteor.publishComposite('all-entidade', function(search,page){
	return {
		find:function(){
			if (!search) search = {};
			if (!page) page = 1;
			var pages = Meteor.settings.public.paginator.itemsPerPage;
			Counts.publish(this,'all-entidade',Entidade.find(search), { noReady: true });
			var entidades = Entidade.find(search,{sort:{date:-1},limit:pages,skip:(page-1)*pages});
			return entidades;
		},
		children:[
			{
				find:function(entidade){
					return EntidadeModulo.find({entidade_id:entidade._id});
				},
				children:[
					{
						find:function(entMod){
							return Modulo.find({_id:entMod.modulo_id});
						}
					}
				]
			}
		]
	}
});

Meteor.publishComposite('one-entidade', function(id){
	return {
		find:function(){
			var entidade = Entidade.find(id);
			return entidade;
		},
		children:[
			{
				find:function(entidade){
					return EntidadeModulo.find({entidade_id:entidade._id});
				},
				children:[
					{
						find:function(entMod){
							return Modulo.find({_id:entMod.modulo_id});
						}
					}
				]
			}
		]
	}
});
